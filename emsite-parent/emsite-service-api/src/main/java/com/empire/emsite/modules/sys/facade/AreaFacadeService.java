/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.facade;

import java.util.List;

import com.empire.emsite.modules.sys.entity.Area;

/**
 * 类AreaFacadeService.java的实现描述：区域FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:51:12
 */
public interface AreaFacadeService {

    public List<Area> findAllList();

    public void save(Area area);

    public void delete(Area area);

    public Area get(String id);
}
