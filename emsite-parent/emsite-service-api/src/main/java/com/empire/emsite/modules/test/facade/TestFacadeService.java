/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.test.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.test.entity.Test;

/**
 * 类TestFacadeService.java的实现描述：测试FacadeService接口
 * 
 * @author arron 2017年9月17日 下午10:01:48
 */
public interface TestFacadeService {
    public List<Test> findList(Test test);

    public Page<Test> findPage(Page<Test> page, Test test);

    public Test get(String id);

    public void save(Test test);

    public void delete(Test test);
}
